# myapp

This project used for react-redux-saga starting point with webpack and babel in production build.

Some module package in this starter template should use exact version:

> react-dev-utils 10.1.0<br />
> react-scripts 3.4.1<br />
> postcss-loader 3.0.0

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

The page will reload if you make edits. You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

## Build with custom public url

Sometimes, You want running app in slashes domain, for example you want to put the project builds in <http://yourdomain.com/myapp/build>, You can do a deployment builds by passing custom path url using `PUBLIC_URL` variable with value `/myapp/build`.

```sh
# In this case, We'll build with PUBLIC_URL values "/<your-current-directory>/build"
THIS_DIRNAME=`echo "${PWD##*/}"`
PUBLIC_URL=/$THIS_DIRNAME/build

rm -rf node_modules package-lock.json yarn.lock && npm cache clear --force
npm install
npm run build
sed -i ".bak" -e "s/\/assets/\/$THIS_DIRNAME\/build\/assets/g" build/static/css/main.*.chunk.css
```
